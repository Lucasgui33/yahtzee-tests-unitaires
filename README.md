# Yahtzee TDD

## Ce projet a été réalisé par Enzo Fonteneau et Lucas Guillotin.

Afin de lancer les tests, veuillez d'abord cloner le projet, puis une fois dans le dosser, faites un :

```bash
npm i
```

Vous pouvez ensuite lancer les tests en faisant :

```bash
npm run yahtzee
```
