import { assert, expect } from "chai";
import * as sinon from 'sinon';
import { Yahtzee } from "./yahtzee";

describe('Yahtzee', () => {
    const yahtzeeMock = sinon.createStubInstance(Yahtzee);
    yahtzeeMock.checkDice.returns();

    describe('Check dice', () => {
        it('test dice not ok', () => {
            const yahtzee = new Yahtzee();
    
            expect(() => yahtzee.checkDice([2,8,5,4,3])).to.throw(Error);
        });
        it('test dice ok', () => {
            const yahtzee = new Yahtzee();
    
            expect(() => yahtzee.checkDice([2,5,5,4,3])).to.not.throw(Error);
        });
    });
    describe('Test 1', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([2,5,5,4,3], 1);
            assert.equal(yahtzee.scores['1'], 0);
        });
        it('Test nb 1',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([2,1,5,1,3], 1);
            assert.equal(yahtzee.scores['1'], 2);
        });
    });
    describe('Test 2', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([1,5,5,4,3], 2);
            assert.equal(yahtzee.scores['2'], 0);
        });
        it('Test nb 2',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([2,2,5,4,2], 2);
            assert.equal(yahtzee.scores['2'], 6);
        });
    });
    describe('Test 3', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([1,5,5,4,2], 3);
            assert.equal(yahtzee.scores['3'], 0);
        });
        it('Test nb 3',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([3,3,5,4,3], 3);
            assert.equal(yahtzee.scores['3'], 9);
        });
    });
    describe('Test 4', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([1,5,5,2,3], 4);
            assert.equal(yahtzee.scores['4'], 0);
        });
        it('Test nb 4',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([4,2,5,4,2], 4);
            assert.equal(yahtzee.scores['4'], 8);
        });
    });
    describe('Test 5', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([1,2,6,4,3], 5);
            assert.equal(yahtzee.scores['5'], 0);
        });
        it('Test nb 5',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([2,5,5,4,2], 5);
            assert.equal(yahtzee.scores['5'], 10);
        });
    });
    describe('Test 6', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([1,5,5,4,3], 6);
            assert.equal(yahtzee.scores['6'], 0);
        });
        it('Test nb 6',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setNumbers([2,6,5,6,6], 6);
            assert.equal(yahtzee.scores['6'], 18);
        });
    });
    describe('Test brelan', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setBrelanCarreYahtzee([1,5,5,4,3], 3);
            assert.equal(yahtzee.scores['brelan'], 0);
        });
        it('Test brelan ok',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setBrelanCarreYahtzee([2,6,5,6,6], 3);
            assert.equal(yahtzee.scores['brelan'], 25);
        });
    });
    describe('Test carré', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setBrelanCarreYahtzee([1,5,5,4,3], 4);
            assert.equal(yahtzee.scores['carre'], 0);
        });
        it('Test carré ok',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setBrelanCarreYahtzee([2,3,3,3,3], 4);
            assert.equal(yahtzee.scores['carre'], 14);
        });
    });
    describe('Test yahtzee', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setBrelanCarreYahtzee([1,5,5,4,3], 5);
            assert.equal(yahtzee.scores['yahtzee'], 0);
        });
        it('Test yahtzee ok',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setBrelanCarreYahtzee([3,3,3,3,3], 5);
            assert.equal(yahtzee.scores['yahtzee'], 50);
        });
    });
    describe('Test full house', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setFullHouse([1,5,5,4,3]);
            assert.equal(yahtzee.scores['full house'], 0);
        });
        it('Test full house ok',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setFullHouse([2,2,3,3,3]);
            assert.equal(yahtzee.scores['full house'], 25);
        });
    });
    describe('Test small straight', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setSmallStraight([1,3,3,4,3]);
            assert.equal(yahtzee.scores['small straight'], 0);
        });
        it('Test large straight ok',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setSmallStraight([4,2,3,1,5]);
            assert.equal(yahtzee.scores['small straight'], 30);
        });
    });
    describe('Test large straight', () => {
        it('Test bad combinaison',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setLargeStraight([1,3,3,4,3]);
            assert.equal(yahtzee.scores['large straight'], 0);
        });
        it('Test large straight ok',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setLargeStraight([4,2,3,1,5]);
            assert.equal(yahtzee.scores['large straight'], 40);
        });
    });
    describe('Test luck', () => {
        it('Test luck ok',() => {
            const yahtzee = new Yahtzee();
            yahtzee.setLuck([4,2,3,1,3]);
            assert.equal(yahtzee.scores['luck'], 13);
        });
    });
    describe('Test sum', () => {
        it('Test sum without bonus',() => {
            const yahtzee = new Yahtzee();
            yahtzee.sum([2,4,6,4,10,12]);
            assert.equal(yahtzee.scores['sum'], 38);
        });
        it('Test sum with bonus',() => {
            const yahtzee = new Yahtzee();
            yahtzee.sum([2,8,9,12,20,18]);
            assert.equal(yahtzee.scores['bonus'], 35)
            assert.equal(yahtzee.scores['sum'], 69);
        });
    }); 
})