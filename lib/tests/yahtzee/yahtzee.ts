export class Yahtzee {
    scores: {} = {};

    checkDice(dice: number[]): void {
        if(dice.some((die) =>{
            return ![1,2,3,4,5,6].includes(die)
        })){
            throw new Error("Un des nombres n'est pas compris entre 1 et 6.");
        }
    }

    setNumbers(dice: number[], nb: number): void{
        let score = 0;

        dice.forEach((die) => {
            if(die === nb) score+= nb;
        });
        this.scores[`${nb}`] = score;
    }

    setBrelanCarreYahtzee(dice: number[], num: number): void{
        let score = 0;
        let countValeurs = [0,0,0,0,0,0];

        dice.forEach((die) => {
            countValeurs[die - 1] += 1;
        })

        if(countValeurs.includes(num)){
            // Additionne les valeurs du tableau (score de tous les dés)
            if(num === 5) score = 50;
            else{
                score = dice.reduce((acc,val) => acc + val, 0);  
            }
        }
        if(num === 3) this.scores['brelan'] = score;
        else if(num === 4) this.scores['carre'] = score;
        else this.scores['yahtzee'] = score;
    }

    setFullHouse(dice: number[]): void{
        let score = 0;
        let countValeurs = [0,0,0,0,0,0];

        dice.forEach((die) => {
            countValeurs[die - 1] += 1;
        });

        if(countValeurs.includes(2) && countValeurs.includes(3)){
            score = 25;
        }

        this.scores['full house'] = score;
    }

    setSmallStraight(dice: number[]): void{
        let score = 0;
        
        if(dice.includes(1) && dice.includes(2) && dice.includes(3) && dice.includes(4)) score = 30;
        else if(dice.includes(2) && dice.includes(3) && dice.includes(4) && dice.includes(5)) score = 30;
        else if(dice.includes(3) && dice.includes(4) && dice.includes(5) && dice.includes(6)) score = 30;
        
        this.scores['small straight'] = score;
    }

    setLargeStraight(dice: number[]): void{
        let score = 0;
        let countValeurs = [0,0,0,0,0,0];

        dice.forEach((die) => {
            countValeurs[die - 1] += 1;
        })

        // si au max 1 seul element > 1
        let elementsSupOne = countValeurs.filter((i) => i > 1);
        // countValeurs index 2,3,4,5 soient tous à au moins 1
        let condition2 = [2,3,4,5].every((nb) => countValeurs[nb - 1] > 0);

        if(!(elementsSupOne.length > 1) && condition2){
            score = 40;
        }

        this.scores['large straight'] = score;
    }

    setLuck(dice: number[]): void{
        let score = dice.reduce((acc,val) => acc + val, 0);

        this.scores['luck'] = score;
    }

    sum(results: number[]): void{
        let score = results.reduce((acc,val) => acc + val, 0);

        if(score >= 63){
            this.scores['bonus'] = 35;
        }

        this.scores['sum'] = score;
    }
}